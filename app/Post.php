<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // only things that you can put in DB
    protected $fillable = ['title','body'];
}
